# Translation of katekonsoleplugin to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2007, 2008, 2009, 2010, 2015, 2018, 2020, 2021, 2023.
# Eirik U. Birkeland <eirbir@gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: katekonsoleplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-25 00:40+0000\n"
"PO-Revision-Date: 2023-05-27 14:34+0200\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.04.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: kateconsole.cpp:70
#, kde-format
msgid "You do not have enough karma to access a shell or terminal emulation"
msgstr ""
"Du har ikkje god nok karma til å få tilgang til eit skal eller ei "
"terminalemulering"

#: kateconsole.cpp:118 kateconsole.cpp:148 kateconsole.cpp:769
#, kde-format
msgid "Terminal"
msgstr "Terminal"

#: kateconsole.cpp:157
#, kde-format
msgctxt "@action"
msgid "&Pipe to Terminal"
msgstr "&Send i røyr til terminalen"

#: kateconsole.cpp:161
#, kde-format
msgctxt "@action"
msgid "S&ynchronize Terminal with Current Document"
msgstr "S&ynkroniser terminalen med dokumentet"

#: kateconsole.cpp:165
#, kde-format
msgctxt "@action"
msgid "Run Current Document"
msgstr "Køyr gjeldande dokument"

#: kateconsole.cpp:170 kateconsole.cpp:556
#, kde-format
msgctxt "@action"
msgid "S&how Terminal Panel"
msgstr "Vis &terminalrute"

#: kateconsole.cpp:176
#, kde-format
msgctxt "@action"
msgid "&Focus Terminal Panel"
msgstr "&Fokus på terminalruta"

#: kateconsole.cpp:182
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "&Hide Terminal Panel"
msgctxt "@action"
msgid "&Split Terminal Vertically"
msgstr "&Gøym terminalruta"

#: kateconsole.cpp:187
#, kde-format
msgctxt "@action"
msgid "&Split Terminal Horizontally"
msgstr ""

#: kateconsole.cpp:192
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "S&how Terminal Panel"
msgctxt "@action"
msgid "&New Terminal Tab"
msgstr "Vis &terminalrute"

#: kateconsole.cpp:355
#, kde-format
msgid ""
"Konsole not installed. Please install konsole to be able to use the terminal."
msgstr ""
"Konsole er ikkje installert. Installer Konsole viss du vil bruka terminalen."

#: kateconsole.cpp:431
#, kde-format
msgid ""
"Do you really want to pipe the text to the console? This will execute any "
"contained commands with your user rights."
msgstr ""
"Vil du verkeleg senda i røyr til terminalen? Dette vil føra til at "
"eventuelle kommandoar som ligg i teksten vert køyrde med dine brukarrettar."

#: kateconsole.cpp:432
#, kde-format
msgid "Pipe to Terminal?"
msgstr "Vil du senda i røyr til terminalen?"

#: kateconsole.cpp:433
#, kde-format
msgid "Pipe to Terminal"
msgstr "Send i røyr til terminalen"

#: kateconsole.cpp:497
#, kde-format
msgid "Not a local file: '%1'"
msgstr "Ikkje ei lokal fil: «%1»"

#: kateconsole.cpp:530
#, kde-format
msgid ""
"Do you really want to Run the document ?\n"
"This will execute the following command,\n"
"with your user rights, in the terminal:\n"
"'%1'"
msgstr ""
"Er du sikker på at du vil køyra dokumentet?\n"
"Dette vil køyra følgjande kommando, med\n"
"dine brukarrettar, i terminalen:\n"
"«%1»"

#: kateconsole.cpp:537
#, kde-format
msgid "Run in Terminal?"
msgstr "Køyra i terminal?"

#: kateconsole.cpp:538
#, kde-format
msgid "Run"
msgstr "Køyr"

#: kateconsole.cpp:553
#, kde-format
msgctxt "@action"
msgid "&Hide Terminal Panel"
msgstr "&Gøym terminalruta"

#: kateconsole.cpp:564
#, kde-format
msgid "Defocus Terminal Panel"
msgstr "Fokus vekk frå terminalruta"

#: kateconsole.cpp:565 kateconsole.cpp:566
#, kde-format
msgid "Focus Terminal Panel"
msgstr "Fokus på terminalruta"

#: kateconsole.cpp:677
#, kde-format
msgid "Terminal Synchronization Mode"
msgstr ""

#: kateconsole.cpp:680
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "S&ynchronize Terminal with Current Document"
msgid "&Don't synchronize terminal tab with current document"
msgstr "S&ynkroniser terminalen med dokumentet"

#: kateconsole.cpp:683
#, fuzzy, kde-format
#| msgid ""
#| "&Automatically synchronize the terminal with the current document when "
#| "possible"
msgid ""
"&Automatically synchronize the current terminal tab with the current "
"document when possible"
msgstr ""
"&Synkroniser terminalen automatisk med det gjeldande dokumentet når mogleg"

#: kateconsole.cpp:686
#, fuzzy, kde-format
#| msgid ""
#| "&Automatically synchronize the terminal with the current document when "
#| "possible"
msgid ""
"&Automatically create a terminal tab for the directory of the current "
"document and switch to it"
msgstr ""
"&Synkroniser terminalen automatisk med det gjeldande dokumentet når mogleg"

#: kateconsole.cpp:693 kateconsole.cpp:714
#, kde-format
msgid "Run in terminal"
msgstr "Køyr i terminal"

#: kateconsole.cpp:695
#, kde-format
msgid "&Remove extension"
msgstr "&Fjern etternamn"

#: kateconsole.cpp:700
#, kde-format
msgid "Prefix:"
msgstr "Prefiks:"

#: kateconsole.cpp:708
#, kde-format
msgid "&Show warning next time"
msgstr "&Vis åtvaring neste gong"

#: kateconsole.cpp:710
#, kde-format
msgid ""
"The next time '%1' is executed, make sure a warning window will pop up, "
"displaying the command to be sent to terminal, for review."
msgstr ""
"Neste gong «%1» vert køyrt, vis eit åtvaringsvindauge med informasjon om "
"kommandoen som vert sendt til terminalen, for eventuell godkjenning."

#: kateconsole.cpp:721
#, kde-format
msgid "Set &EDITOR environment variable to 'kate -b'"
msgstr "Set miljøvariabelen &EDITOR til «kate -b»"

#: kateconsole.cpp:724
#, kde-format
msgid ""
"Important: The document has to be closed to make the console application "
"continue"
msgstr ""
"Viktig: Dokumentet må lukkast for at konsollprogrammet skal halda fram."

#: kateconsole.cpp:727
#, kde-format
msgid "Hide Konsole on pressing 'Esc'"
msgstr "Gøym Konsole ved trykk på «Escape»"

#: kateconsole.cpp:730
#, kde-format
msgid ""
"This may cause issues with terminal apps that use Esc key, for e.g., vim. "
"Add these apps in the input below (Comma separated list)"
msgstr ""
"Dette kan føra til problem med terminal­progarm som brukar «Escape»-tasten, "
"for eksempel Vim. Legg til desse programma nedanfor (skil programnamna med "
"komma)."

#: kateconsole.cpp:774
#, kde-format
msgid "Terminal Settings"
msgstr "Terminalinnstillingar"

#. i18n: ectx: Menu (tools)
#: ui.rc:6
#, kde-format
msgid "&Tools"
msgstr "V&erktøy"
